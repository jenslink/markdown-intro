# Markdown

## $ whomai

* Freelancer
* Linux seit es das auf 35 Disketten gab
* Netzwerkkram seit 2000
* IPv6 seit 2007
* Automatisierung
* Orga Adminstammtisch Berlin: https://www.flarp.de (zur Zeit virtuell)

## Zielgruppe

Alle die keine Angst vor der Kommandozeile haben und denen GUIs die meiste Zeit nur im Weg
sind.

## Markdown (I)


>  Markdown ist eine vereinfachte Auszeichnungssprache [...]  Ziel von Markdown ist,
>  dass schon die Ausgangsform ohne weitere Konvertierung leicht lesbar ist.

*Q: https://de.wikipedia.org/wiki/Markdown*

## Markdown (II)

* Reine Textdateien
* einfach zu schreiben, einfach zu lesen
* flexibel bei den Ausgabeformaten
* Einfach versionierbar

## Wozu?

* Software Dokumentation
* "Wikis"
* Webseiten
* Präsentationen (So wie diese hier)
* Briefe und Rechnungen

## Tools

* $EDITOR - Bei mir vim mit einigen Plugins
* git für die Versionskontrolle
* pandoc für
  * Präsentationen
  * Briefe / Rechnungen
  * andere Dokumente
* gohugo für Webseiten
* github / gitlab / gogs für Softwaredokumentation und "Wikis"

## Überschriften

```
# 1 Grad
## 2 Grad
### 3 Grad
```

## Auszeichnungen

* ```**fett**```  **fett**
* ```*kursiv*```  *kursiv*
* ```~~durchgestrichen~~ ```  ~~durchgestrichen~~

## Zitate

```
>  Markdown ist eine vereinfachte Auszeichnungssprache [...]
>  Ziel von Markdown ist, dass schon die Ausgangsform ohne weitere
>  Konvertierung leicht lesbar ist.
```

>  Markdown ist eine vereinfachte Auszeichnungssprache [...]
>  Ziel von Markdown ist, dass schon die Ausgangsform ohne weitere
>  Konvertierung leicht lesbar ist.

## Aufzählungen

```
1. A
2. B
16. D
3. C
```

1. A
2. B
16. D
3. C

## Aufzählungen

```
- A
- B
- C
```

- A
- B
- C

## Tabellen

```
| Kunde | Stunden |
| ----- | ------- |
|   A   |   80    |
|   B   |   20    |
```

| Kunde | Stunden |
| ----- | ------- |
|   A   |   80    |
|   B   |   20    |





